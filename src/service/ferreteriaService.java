/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;


import dao.ferreteriaDao;
import domain.*;
import java.io.PrintStream;
import java.util.List;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jorgenio
 */
public class ferreteriaService {
    ferreteriaDao dao=new ferreteriaDao();

    public ferreteriaDao getDao() {
        return dao;
    }

    public void setDao(ferreteriaDao dao) {
        this.dao = dao;
    }
   
    
   
    
    
    public void Listarusuarios(JTable tabla) {
    
        List listausuario=dao.getLista("Usuarios");
       
        Vector<String> tableHeaders = new Vector<String>();
        Vector tableData = new Vector();
        tableHeaders.add("Ci");
        tableHeaders.add("Nombre");
        tableHeaders.add("A. Paterno");
        tableHeaders.add("A. Materno");

        if(listausuario!=null) 
        for (Object o : listausuario) {
            Usuarios usuario = (Usuarios) o;
            Vector<Object> oneRow = new Vector<Object>();
            oneRow.add(usuario.getCi());
            oneRow.add(usuario.getNombre());
            oneRow.add(usuario.getAp());
            oneRow.add(usuario.getAm());
            tableData.add(oneRow);
        }
        tabla.setModel(new DefaultTableModel(tableData, tableHeaders){
                    public boolean isCellEditable(int row, int column){
                    return false;
                              }
            }
                    );

    }    
    


}
