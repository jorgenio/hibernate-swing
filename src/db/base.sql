-- Table: usuarios

-- DROP TABLE usuarios;

CREATE TABLE usuarios
(
  ci integer NOT NULL,
  nombre character varying(60) NOT NULL,
  ap character varying(60),
  am character varying(60),
  CONSTRAINT usuarios_pkey PRIMARY KEY (ci)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE usuarios OWNER TO postgres;
