/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domain.*;
import java.util.List;
import org.hibernate.Session;
import util.HibernateUtil;

/**
 *
 * @author jorgenio
 */
public class ferreteriaDao {
    
 /**   public Datos getLogear(String login ,String clave) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List lista = session.createQuery("from Datos where login='"+login+"' and clave='"+clave+"'").list();
        if(lista.size()>0){
                return (Datos) lista.get(0);
        }
        return null;
    }
    **/
    public List getLista(String tabla) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List lista = session.createQuery("from "+tabla).list();
        session.close();
        if(lista.size()>0){
                return lista;
        }
        return null;
    }
    public List getLotes(int codpro) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List lista = session.createQuery("from Lote where codpro="+codpro).list();
        session.close();
        if(lista.size()>0){
                return lista;
        }
        return null;
    }
 

    public void saveOrUpdateUsuarios(Usuarios usuarios){
        System.out.print("guardando");
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.saveOrUpdate(usuarios);
        session.beginTransaction().commit();
        session.close();
    }

      public Usuarios getUsuarios(int codusuario){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction().commit();
        Usuarios usuarios=(Usuarios) session.get(Usuarios.class, codusuario);
        session.close();
        return usuarios; 
    }

    
    

}
